import os
import re
import csv

baconFile = open('result.txt', 'r')
baconContent = baconFile.read()
baconFile.close()

og = baconContent

obj = re.compile(r'(/scratch/(.|\n)*?)/scratch')
result = obj.search(baconContent)

sections = []

while result:
    result = obj.search(baconContent)
    try:
        extract = result.group(1)
        sections.append(extract)
        baconContent = baconContent.replace(extract, '')
    except:
        result = False

fileNameRegex = re.compile(r'/scratch.*(frame.*.jpg)')
filenames = []

labelRegex = re.compile(r'person:\s(\d+)%.*left_x:\s*(\d+)\s*top_y:\s*(\d+)\s*width:\s*(\d+)\s*height:\s*(\d+)')

with open('bounding_boxes.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

    for section in sections:
        fileNameResult = fileNameRegex.search(section)
        filenames.append(fileNameResult.group(1))
        labelResult = labelRegex.findall(section)
        for result in labelResult:
            print([fileNameResult.group(1), result[0], result[1], result[2], result[3], result[4]])
            spamwriter.writerow([fileNameResult.group(1), result[0], result[1], result[2], result[3], result[4]])
